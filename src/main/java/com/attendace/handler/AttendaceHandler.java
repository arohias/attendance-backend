package com.attendace.handler;

import com.attendace.bean.DayinRequest;
import com.attendace.bean.HolidayRequest;
import com.attendace.bean.loginRequest;
import com.attendace.bean.WsResponse;

public interface AttendaceHandler {

	WsResponse add(loginRequest newsRequest) throws com.attendace.exception.Attendace;

	WsResponse addHoliday(HolidayRequest newsRequest) throws com.attendace.exception.Attendace;

	WsResponse getHolidayList() throws com.attendace.exception.Attendace;

	WsResponse dayIn(DayinRequest dayinRequest) throws com.attendace.exception.Attendace;

	WsResponse dayOut(DayinRequest dayinRequest) throws com.attendace.exception.Attendace;

	WsResponse checkDayInDayout(DayinRequest dayinRequest) throws com.attendace.exception.Attendace;

	WsResponse getUserList() throws com.attendace.exception.Attendace;

	WsResponse getReport(String ReportType) throws com.attendace.exception.Attendace;
	
}
