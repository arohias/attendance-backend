package com.attendace.handler;

import com.attendace.bean.*;
import com.attendace.enums.ErrorMessageEnum;

import com.attendace.exception.Attendace;
import com.attendace.model.Attendance;
import com.attendace.model.Holiday;
import com.attendace.model.User;
import com.attendace.repository.AttendanceRepository;
import com.attendace.repository.HolidayRepository;
import com.attendace.repository.UserRepository;
import com.attendace.util.UtilMethod;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.attendace.constants.ResponseMessage;

import java.time.DayOfWeek;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lapto
 */
@Component
public class AttendaceHandlerImpl implements AttendaceHandler {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HolidayRepository holidayRepository;

    @Autowired
    private AttendanceRepository attendanceRepository;


    /**
     * Method is used to get current weather
     *
     * @param newsRequest
     */
    @Override
    public WsResponse add(loginRequest newsRequest) throws com.attendace.exception.Attendace {
        final WsResponse response = new WsResponse();
        User login = new User();
        login = userRepository.findByEmailAndPassword(newsRequest.getEmail(), newsRequest.getPassword());
        if (login == null)
            throw new com.attendace.exception.Attendace(ErrorMessageEnum.APP001.name(), ErrorMessageEnum.APP001.value());
        response.setMessage(ResponseMessage.SUCCESS);
        login.setPassword(null);
        response.setStatus((login == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(login);
        return response;

    }

    @Override
    public WsResponse addHoliday(HolidayRequest holidayRequest) throws Attendace {
        final WsResponse response = new WsResponse();
        Holiday holiday = new Holiday();
        BeanUtils.copyProperties(holidayRequest, holiday);
        holiday = holidayRepository.save(holiday);
        response.setMessage(ResponseMessage.SUCCESS);
        response.setStatus((holiday == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(holiday);
        return response;
    }

    @Override
    public WsResponse getHolidayList() throws Attendace {
        final WsResponse response = new WsResponse();
        List<Holiday> holiday = holidayRepository.findAll();
        response.setMessage(ResponseMessage.SUCCESS);
        response.setStatus((holiday == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(holiday);
        return response;
    }

    @Override
    public WsResponse dayIn(DayinRequest dayinRequest) throws Attendace {
        final WsResponse response = new WsResponse();
        Attendance attendance = new Attendance();
        attendance.setDate(new Date());
        attendance.setUserId(dayinRequest.getUserId());
        attendance.setDayin(new Date());
        attendance = attendanceRepository.save(attendance);
        response.setMessage(ResponseMessage.SUCCESS);
        response.setStatus((attendance == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(attendance);
        return response;
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    @Override
    public WsResponse dayOut(DayinRequest dayinRequest) throws Attendace {
        final WsResponse response = new WsResponse();
        Attendance attendance = new Attendance();
        attendance.setDate(new Date());
        attendance.setId(dayinRequest.getUserId());
        Date existDate = removeTime(new Date());
        List<Attendance> all = attendanceRepository.findAll();
        List<Attendance> byUserId = all.stream().filter(data -> data.getDate().getTime() == existDate.getTime() && data.getUserId() == dayinRequest.getUserId()).collect(Collectors.toList());
        if (byUserId.isEmpty())
            throw new com.attendace.exception.Attendace(ErrorMessageEnum.APP002.name(), ErrorMessageEnum.APP002.value());
        byUserId.get(0).setDayout(new Date());
        byUserId.get(0).setSummary(dayinRequest.getSummary());
        attendance = attendanceRepository.save(byUserId.get(0));
        response.setMessage(ResponseMessage.SUCCESS);
        response.setStatus((attendance == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(attendance);
        return response;
    }

    @Override
    public WsResponse checkDayInDayout(DayinRequest dayinRequest) throws Attendace {
        final WsResponse response = new WsResponse();
        response.setMessage(ResponseMessage.SUCCESS);
        List<Attendance> all = attendanceRepository.findAll();
        Date existDate = removeTime(new Date());
        DayInDayoutResponse dayInDayout = new DayInDayoutResponse();
        List<Attendance> byUserId = all.stream().filter(data -> data.getDate().getTime() == existDate.getTime() && data.getUserId() == dayinRequest.getUserId()).collect(Collectors.toList());
        if (!byUserId.isEmpty()) {
            Attendance findData = byUserId.get(0);
            if (findData.getDayin() != null)
                dayInDayout.setDayIn(true);
            if (findData.getDayout() != null)
                dayInDayout.setDayOut(true);
        }
        response.setStatus((dayInDayout == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(dayInDayout);
        return response;

    }

    @Override
    public WsResponse getUserList() throws Attendace {
        final WsResponse response = new WsResponse();
        response.setMessage(ResponseMessage.SUCCESS);
        List<User> allUser = userRepository.findAll().stream().filter(data -> data.getRole() != "ADMIN").collect(Collectors.toList());
        response.setStatus((allUser == null) ? HttpStatus.NO_CONTENT.value() : HttpStatus.OK.value());
        response.setData(allUser);
        return response;
    }

    @Override
    public WsResponse getReport(String ReportType) throws Attendace {
        Date firstDayOfWeek=null;
        Date lastDayOfWeek=null;
        if (ReportType.equalsIgnoreCase("weekly")) {
              firstDayOfWeek = UtilMethod.getWeekStartDate();
              lastDayOfWeek = UtilMethod.getWeekStartDate();
        }
        if (ReportType.equalsIgnoreCase("monthly")) {
            firstDayOfWeek = UtilMethod.getMonthStartDate();
            lastDayOfWeek = UtilMethod.getMonthEndDate();
        }

        return null;
    }

}
