package com.attendace.controller;

import com.attendace.bean.DayinRequest;
import com.attendace.bean.HolidayRequest;
import com.attendace.bean.loginRequest;
import com.attendace.exception.Attendace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.attendace.bean.WsResponse;
import com.attendace.handler.AttendaceHandler;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "/api/v1/", description = " attendance API")
@CrossOrigin(origins = "*",allowedHeaders = "*")
@RequestMapping(value = "/api/v1/attendance")
public class AttendaceController {
    @Autowired
    private AttendaceHandler attemdaceHandler;

    /**
     * @param newsRequest
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "API to login")
    public ResponseEntity<WsResponse> login(@RequestBody loginRequest newsRequest)
            throws com.attendace.exception.Attendace {
        final WsResponse response = attemdaceHandler.add(newsRequest);
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

    @PostMapping("/holiday")
    @ApiOperation(value = "API to Add holiday")
    public ResponseEntity<WsResponse> addNews(@RequestBody HolidayRequest holidayRequest)
            throws com.attendace.exception.Attendace {
        final WsResponse response = attemdaceHandler.addHoliday(holidayRequest);
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

    @GetMapping("/holiday/getAll")
    @ApiOperation(value = "API to get holiday")
    public ResponseEntity<WsResponse> getHolidyList()
            throws Attendace {
        final WsResponse response = attemdaceHandler.getHolidayList();
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

    @PostMapping("/dayin")
    @ApiOperation(value = "API to day in")
    public ResponseEntity<WsResponse> dayin(@RequestBody DayinRequest dayinRequest)
            throws Attendace {
        final WsResponse response = attemdaceHandler.dayIn(dayinRequest);
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

    @PatchMapping("/dayout")
    @ApiOperation(value = "API to day in")
    public ResponseEntity<WsResponse> dayout(@RequestBody DayinRequest dayinRequest)
            throws Attendace {
        final WsResponse response = attemdaceHandler.dayOut(dayinRequest);
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }


    @PostMapping("/check_dayin_dayout")
    @ApiOperation(value = "API to get day in day out")
    public ResponseEntity<WsResponse> checkDayIn(@RequestBody DayinRequest dayinRequest)
            throws Attendace {
        final WsResponse response = attemdaceHandler.checkDayInDayout(dayinRequest);
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }


    @GetMapping("/users/getAll")
    @ApiOperation(value = "API to get All users")
    public ResponseEntity<WsResponse> getUserList()
            throws Attendace {
        final WsResponse response = attemdaceHandler.getUserList();
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

    @GetMapping("/report/{reportType}")
    @ApiOperation(value = "API to get report based on reportType, WEEKLY,MONTHLY")
    public ResponseEntity<WsResponse> getReport(@PathVariable("reportType") String reportType)
            throws Attendace {
        final WsResponse response = attemdaceHandler.getUserList();
        return new ResponseEntity<WsResponse>(response, HttpStatus.OK);
    }

}
