package com.attendace.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="attendance")
@Getter
@Setter
@NoArgsConstructor
public class Attendance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="userId")
	private Long userId;

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	@Column(name="dayin")
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date  dayin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dayout")
	private java.util.Date  dayout;

	@Column(name="summary")
	private String summary;

}
