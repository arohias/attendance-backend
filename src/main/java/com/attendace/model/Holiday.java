package com.attendace.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="Holiday")
@Getter
@Setter
@NoArgsConstructor
public class Holiday {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	@Column(name="purpose")
	private String purpose;

}
