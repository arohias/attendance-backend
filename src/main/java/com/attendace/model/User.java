package com.attendace.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="Users")
@Getter
@Setter
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="password")
	private String password;

	@Column(name="email", nullable=false, length=200)
	private String email;

	@Column(name="name")
	private String name;

	@Column(name="role")
	private String role;

}
