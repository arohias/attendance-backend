package com.attendace.repository;


import com.attendace.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository
        extends JpaRepository<User, Long> {
//    @Query("select rc from User rc where  rc.email = ?1 and  rc.password=?2")
     User findByEmailAndPassword(String email,String password);

}