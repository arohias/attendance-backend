package com.attendace.repository;

import com.attendace.model.Holiday;
//import com.news.model.RegisterNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HolidayRepository
        extends JpaRepository<Holiday, Long> {
//    @Query("select rc from Holiday rc where  rc.NewsId = ?1")
//    List<Holiday> findRegisterCommentByNewsId(Long NewsId);
}