package com.attendace.repository;

import com.attendace.model.Attendance;
import com.attendace.model.Holiday;
import com.attendace.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;


@Repository
public interface AttendanceRepository
        extends JpaRepository<Attendance, Long> {
//  @Query("select rc from Attendance rc where  rc.date = ?1 and rc.userId=?2")
//   Attendance findByDateAndUserId(Date date,Long userId);
}