package com.attendace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AttendaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttendaceApplication.class, args);
	}
}
