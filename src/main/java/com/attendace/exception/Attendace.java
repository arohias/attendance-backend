package com.attendace.exception;


/**
 * @author 
 *
 * Custom Exception class for com.WeatherChallengeException.
 */
public class Attendace extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String errorMessge;

    private String errorCode;


    public Attendace(String errorCode, String errorMessge) {
        this.errorCode = errorCode;
        this.errorMessge = errorMessge;
    }

    public Attendace(String s, String errorMessge, String errorCode) {
        super(s);
        this.errorMessge = errorMessge;
        this.errorCode = errorCode;
    }

    public Attendace(String s, Throwable throwable, String errorMessge, String errorCode) {
        super(s, throwable);
        this.errorMessge = errorMessge;
        this.errorCode = errorCode;
    }

    public Attendace(Throwable throwable, String errorMessge, String errorCode) {
        super(throwable);
        this.errorMessge = errorMessge;
        this.errorCode = errorCode;
    }

    public Attendace(Throwable e) {
        super(e);
    }



    public String getErrorMessge() {
        return errorMessge;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
