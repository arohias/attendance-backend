package com.attendace.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DayInDayoutResponse {
    private Boolean dayIn=false;
    private Boolean dayOut=false;
}
