package com.attendace.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HolidayRequest {
    private String purpose;
    private java.util.Date date;
}
