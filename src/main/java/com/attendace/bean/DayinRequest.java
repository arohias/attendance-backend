package com.attendace.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DayinRequest {
    private Long userId;
    private String summary;
}
